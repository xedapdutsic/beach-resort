import React, { Component } from "react";
import Navbar from "./components/Navbar";
import Home from "./pages/Home";
import Rooms from "./pages/Rooms";
import SingleRoom from "./pages/SingleRoom";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import "./App.css";

export default class App extends Component {
  render() {
    return (
      <Router>
        <Navbar />
        <>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/rooms/" component={Rooms} />
            <Route exact path="/rooms/:slug" component={SingleRoom} />
          </Switch>
        </>
      </Router>
    );
  }
}
