import React from "react";
import { RoomConsumer } from "../context/Context";
import Loading from "./Loading";
import RoomsFilter from "./RoomFilter";
import RoomsList from "./RoomList";
export default function RoomContainer() {
  return <RoomConsumer>
      {value => {
          const {rooms, sortedRooms, loading} = value
          // console.log(sortedRooms); 
          if(loading){
            return <Loading />
          }
          return(
              <div>
                  <RoomsFilter rooms={rooms} />
                  <RoomsList rooms={sortedRooms} />
              </div>
          )
      }}
      </RoomConsumer>;
}
