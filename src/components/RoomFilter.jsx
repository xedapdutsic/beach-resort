import React from "react";
import { RoomConsumer } from "../context/Context";
import Title from "../components/Title";

const getUnique = (items, value) => {
  return [...new Set(items.map((item) => item[value]))];
};

const RoomFilter = ({ rooms }) => {
  return (
    <RoomConsumer>
      {(value) => {
        //type
        let types = getUnique(rooms, "type");
        types = ["all", ...types];
        types = types.map((item, index) => (
          <option key={index} value={item}>
            {item}
          </option>
        ));

        // capacity
        let people = getUnique(rooms, "capacity");
        people = people.map((item, index) => (
          <option key={index} value={item}>
            {item}
          </option>
        ));
        const {
          handleChange,
          type,
          capacity,
          price,
          minPrice,
          maxPrice,
          minSize,
          maxSize,
        } = value;
        return (
          <div className="filter-container">
            <Title title="search room" />
            <form className="filter-form">
              <div className="form-group">
                <label htmlFor="type">room type</label>
                <select
                  name="type"
                  id="type"
                  value={type}
                  className="form-control"
                  onChange={handleChange}
                >
                  {types}
                </select>
              </div>

              <div className="form-group">
                <label htmlFor="capacity">Guests</label>
                <select
                  name="capacity"
                  id="capacity"
                  value={capacity}
                  className="form-control"
                  onChange={handleChange}
                >
                  {people}
                </select>
              </div>

              <div className="form-group">
                <label htmlFor="price">Price {price}</label>
                <input
                  type="range"
                  name="price"
                  id="price"
                  min={minPrice}
                  max={maxPrice}
                  value={price}
                  onChange={handleChange}
                  className="form-control"
                />
              </div>

              <div className="form-group">
                <label htmlFor="price">room size </label>
                <div className="size-inputs">
                  <input
                    type="number"
                    name="minSize"
                    value={minSize}
                    onChange={handleChange}
                    className="size-input"
                  />
                  <input
                    type="number"
                    name="maxSize"
                    value={maxSize}
                    onChange={handleChange}
                    className="size-input"
                  />
                </div>
              </div>

              <div className="form-group">
                <div className="single-extra">
                  <input
                    type="checkbox"
                    name="breakfast"
                    id="breakfast"
                    onChange={handleChange}
                  />
                  <label htmlFor="breakfast">breakfast</label>
                </div>
                <div className="single-extra">
                  <input
                    type="checkbox"
                    name="pets"
                    id="pest"
                    onChange={handleChange}
                  />
                  <label htmlFor="pest">pets</label>
                </div>
              </div>
            </form>
          </div>
        );
      }}
    </RoomConsumer>
  );
};

export default RoomFilter;
