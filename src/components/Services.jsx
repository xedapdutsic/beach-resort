import React, {Component} from "react";
import { FaCocktail, FaHiking, FaShuttleVan, FaBeer } from "react-icons/fa";
import Title from "./Title";

export default class Services extends Component {
  constructor(props) {
    super(props);
    this.state = {
      services: [
        {
          icon: <FaCocktail />,
          title: "Free Cocktails",
          info:
            "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Alias molestias eius libero?",
        },
        {
          icon: <FaHiking />,
          title: "Endless Hiking",
          info:
            "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Alias molestias eius libero?",
        },
        {
          icon: <FaShuttleVan />,
          title: "Free Shuttle",
          info:
            "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Alias molestias eius libero?",
        },
        {
          icon: <FaBeer />,
          title: "Strongest Beer",
          info:
            "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Alias molestias eius libero?",
        },
      ],
    };
  }
  render() {
    return (
      <section className="services">
        <Title title="services" />
        <div className="services-center">
          {this.state.services.map((item) => {
            return (
              <article className="service" key={`item-${item.title}`}>
                <span>{item.icon}</span>
                <h4>{item.title}</h4>
                <p>{item.info}</p>
              </article>
            );
          })}
        </div>
      </section>
    );
  }
}
