import styled from "styled-components";
import defaultImg from "../images/room-1.jpeg";

const StyleHero = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  min-height: 60vh;
  background: url(${(props) => (props.img ? props.img : defaultImg)}) center/cover no-repeat;
`;
export default StyleHero;
